var recordArray;
var idOrderedArray = [];
//FIRST FROM NEWS WILL BE DISPLAYED, IF SMALLER DEVICE IS USED
var lastIdOfDetail = 0;

function htmlToElementsOrText(htmlString, objectToSetType, elementToAppendChildIfText) {
	let div = document.createElement('div');
	div.classList.add("descriptionDivWithHtmlContent");
	div.innerHTML = htmlString.trim();
	
	//FINDS OUT IF IT IS TEXT OR HTML STRUCTURE
	if(div.childElementCount == 0){
		objectToSetType.setDescriptionType("text");
		
		//CREATE ELEMENT AND ADD TO P NODE
		var elementDescription = document.createElement("P");
		elementDescription.classList.add("appendedDescription");
		elementDescription.innerText = htmlString;
		elementToAppendChildIfText.appendChild(elementDescription);
	
		return htmlString;
	}else{
		objectToSetType.setDescriptionType("html");
		return div; 
	}
}

//FILLS INFORMATION OF DETAIL
function fillDetail(recordOfDetail){
	//SETS TITLE TO DETAIL INFORMATION PART
	document.getElementById("headingDetail").innerText = recordOfDetail.getTitle();
	
	//SET DETAIL IN SELECTED FORM
	if(recordOfDetail.getDescriptionType() == "text"){
		//SETS DESCRIPTION TO DETAIL INFORMATION PART
		document.getElementById("descriptionDetailP").innerText = recordOfDetail.getDescription();
		document.getElementById("descriptionDetailP").style.display="inline-block";
	} else {
		//SETS DESCRIPTION TO DETAIL INFORMATION PART
		document.getElementById("descriptionDetail").innerHTML="";
		document.getElementById("descriptionDetail").append(recordOfDetail.getDescription());
		var desDetailP = document.getElementById("descriptionDetailP")
		if(desDetailP != null) { desDetailP.style.display="none"; }
	}
	
	//SETS DATE TO DETAIL INFORMATION PART
	document.getElementById("dateDetail").innerText = recordOfDetail.getDate();
	//SETS PHOTO TO DETAIL INFORMATION PART
	obtainedImageUrl = recordOfDetail.getImageUrl();
	//IF IMAGE URL IS NOT SET OR RECOGNIZED
	if( obtainedImageUrl == ""){
		document.getElementById("photoDetail").style.display="none";
	} else{
		document.getElementById("photoDetail").src = obtainedImageUrl;
		document.getElementById("photoDetail").style.display="block";
	}
	//SETS LINK TO DETAIL INFORMATION PART
	document.getElementById("linkDetail").href = recordOfDetail.getLink();
	
	//document.getElementsByClassName("hiddenForDetail")[idOfDetail]
}

//MANAGES RESIZE TO CHANGE DETAIL BETWEEN ELEMENTS IN DOM - ONLY JAVASCRIPT FUNCTION
window.onresize = function resizeListener(){
	var ele = document.getElementById("detailPart");
	if(typeof ele != "undefined"){
		var vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		if(vw > 1100) {
			document.getElementsByTagName("main")[0].append(ele);
		}
		else{
			ele.style.display="block";
			document.getElementsByClassName("recordOfItem")[lastIdOfDetail].append(ele);
		}
	}
}

//document.window.addEventListener('resize', resizeListener());

//SHOWS DETAIL ACCESSING REQUIRED ITEM IN O(1) AND SET NECCESSARY VALUES
function showDetail(linkElement){
	//GET ITEM IDENTIFIER OF CONTENT WITH REQUIRED RECORD - THIS IDENTIFIER REPRESENTS NUMBER OF ITEM CONTAINER
	//IF DATA ARE CHANGED, THIS VALUE WILL IDENTIFINELLY, OR UP TO RELOAD, POINT TO ITEM CONTAINER WHERE ARE STORED, BECAUSE THERE ARE WRITTEN, 
	//THIS NUMBER WILL BE SAME AS NEW INDEX IN FIELD  
	var idOfDetail = lastIdOfDetail = linkElement.classList.item(0);
	//GET RECORD USING ID VALUE
	var recordOfDetail = recordArray[idOfDetail];
	
	
	var ele = document.getElementById("detailPart");
	//ele.parentElement.removeChild(ele);
	var vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	if(vw > 1100) {

		//FILLS THE DETAIL
		fillDetail(recordOfDetail);
		//ele.parentElement.removeChild(ele);
		document.getElementsByTagName("main")[0].append(ele);
	}
	else{
		fillDetail(recordOfDetail);
		ele.parentElement.removeChild(ele);
		ele.style.display="block";
		document.getElementsByClassName("recordOfItem")[idOfDetail].append(ele);
	}

}

//OVERWRITES FIELDS WHICH REPRESENTS ITEMS WITH NEW SORTED VALUES
function overwriteCreatedAndSortedElementsContent(){
		//ITERATES OVER ALL ITEMS - RECORDS
		for(j=0; j<recordArray.length; j++){
			//SET NEW TITLE CONTENT TO j TITLE FROM ITEM FROM SORTED ARRAY ON POSITION j
			document.getElementsByClassName("appendedTitle")[j].innerText=recordArray[j].getTitle();
			//IF DESCRIPTION IS TEXT ONLY - PROVIDE IN LIST
			if(recordArray[j].getDescriptionType() == "text"){
				//SET NEW DESCRIPTION CONTENT TO j DESCRIPTION FROM ITEM FROM SORTED ARRAY ON POSITION j
				document.getElementsByClassName("appendedDescription")[j].innerText=recordArray[j].getDescription();
			}
			//SET NEW DATE CONTENT TO j DATE FROM ITEM FROM SORTED ARRAY ON POSITION j
			document.getElementsByClassName("appendedDate")[j].innerText=recordArray[j].getDate();		
		}
}

//SORTS IF NECCESSARY AND FILLS ALL PREPARE FIELDS - USES THE SAME XML
function sortAndDisplayResults(selectedValue) {
	if( recordArray != undefined ){
		//SORTS ARRAY
		doSorting(recordArray, selectedValue);
		//OVERWRITES FIELDS WHICH REPRESENTS ITEMS WITH NEW SORTED VALUES
		overwriteCreatedAndSortedElementsContent();
	}
}

function Record(id, timeMs) {
	//ID - SEQUENCE IN XML FILE
	this.id = id;
	//TIME IM MS AFTER 1970
	this.timeMs = timeMs;
	
	this.link;				//LINK TO ANOTHER PAGE
	this.date;				//DATE OF RECORD CREATION
	this.description;		//DESCRIPTION OF RECORD
	this.imageUrl;			//IMAGE URL OF GIVEN PICTURE
	this.title;				//TITLE OF RECORD
	this.descriptionType;	//DESCRIPTION TYPE - TEXT (SUITABLE FOR P ELEMENT) OR HTML (PART OF HTML STRUCTURE)
	
	//GETTERS AND SETTERS FOR ID
	this.getId = function(){ return this.id; };
	this.getTimeMs = function(){ return this.timeMs; };
	//GETTERS AND SETTERS FOR LINK
	this.setLink = function(link){ this.link=link; };
	this.getLink = function(){ return this.link; };
	//GETTERS AND SETTERS FOR ID
	this.setDate = function(date) { this.date=date; };
	this.getDate = function() { return this.date; };
	//GETTERS AND SETTERS FOR DESCRIPTION
	this.setDescription = function(description){ this.description=description; };
	this.getDescription = function() { return this.description; };
	//GETTERS AND SETTERS FOR IMAGE URL
	this.setImageUrl = function(imageUrl) { this.imageUrl=imageUrl; };
	this.getImageUrl = function() { return this.imageUrl; }
	//GETTERS AND SETTERS FOR TITLE
	this.setTitle = function(title) { this.title=title; };
	this.getTitle = function() { return this.title; };
	//GETTERS AND SETTERS FOR DESCRIPTION TYPE
	this.setDescriptionType = function(descriptionType) { this.descriptionType = descriptionType; };
	this.getDescriptionType = function() { return this.descriptionType; };
};

//OBTAINS TITLE USING ID (REPRESENTS ORDER IN ORIGINAL DOC), ADDS CLASS FOR VISUALISATION AND RETURN CONTENT OF TITLE
function prepareTitle(id, elementToAppendChild) {
	var elementTitle = document.createElement("H3");
	var resultTitle = parsedXml.evaluate('rss/channel/item['+id+']/title', parsedXml, null, XPathResult.STRING_TYPE, null);
	elementTitle.innerText = resultTitle.stringValue;
	elementTitle.classList.add("appendedTitle");
	elementToAppendChild.appendChild(elementTitle);
	return elementTitle.innerText;
}

//OBTAINS DESCRIPTION USING ID (REPRESENTS ORDER IN ORIGINAL DOC), ADDS CLASS FOR VISUALISATION AND RETURN CONTENT OF DESCRIPTION
function prepareDescription(id) {
	var resultDescription = parsedXml.evaluate('rss/channel/item['+id+']/description', parsedXml, null, XPathResult.STRING_TYPE, null);	
	return resultDescription.stringValue;
}

//OBTAINS READABLE DATE USING ID (REPRESENTS ORDER IN ORIGINAL DOC), ADDS CLASS FOR VISUALISATION AND RETURN CONTENT OF READABLE DATE
function prepareReadableDate(id, elementToAppendChild) {
	var elementDate = document.createElement("P");
	var resultDate = parsedXml.evaluate('rss/channel/item['+id+']/pubDate', parsedXml, null, XPathResult.STRING_TYPE, null);
	elementDate.classList.add("appendedDate");
					
	var newDate = new Date(resultDate.stringValue)
	var year = newDate.getFullYear();
	var rawMonth = parseInt(newDate.getMonth()) + 1;
	var month = rawMonth < 10 ? '0' + rawMonth : rawMonth;
	var rawDay = parseInt(newDate.getDate());
	var day = rawDay < 10 ? '0' + rawDay : rawDay; 
			
	var readableDateFormat= year + ' ' + month + ' ' + day;
	elementDate.innerText = readableDateFormat;
	elementToAppendChild.appendChild(elementDate);
	return readableDateFormat;
}

//OBTAINS IMAGE URL USING ID (REPRESENTS ORDER IN ORIGINAL DOC), ADDS CLASS FOR VISUALISATION AND RETURNS CONTENT OF LINK
function prepareImageUrl(id){
	var resultPhotoURL = parsedXml.evaluate('rss/channel/item['+id+']/enclosure/@url', parsedXml, null, XPathResult.STRING_TYPE, null);
	return resultPhotoURL.stringValue;
}

//OBTAINS LINK USING ID (REPRESENTS ORDER IN ORIGINAL DOC), ADDS CLASS FOR VISUALISATION AND RETURNS CONTENT OF LINK
function prepareLink(id) {
	var resultLinkToAllContent = parsedXml.evaluate('rss/channel/item['+id+']/link', parsedXml, null, XPathResult.STRING_TYPE, null);
	return resultLinkToAllContent.stringValue;
}

//PREPARES ELEMENTS AS LINK AND ICON, ENABLES USING REFFERENCE TO FIELD BY STORING THIS POSITION
function prepareLinkToDetailWithPicture(positionInField, elementToAppendChild){
	var linkElementWithPicture = document.createElement("A");
	var pictureToLink = document.createElement("IMG");
	pictureToLink.alt = "Show detail of RSS record";
	pictureToLink.src = "rssReaderFiles/pictures/eye-view.svg";
	linkElementWithPicture.appendChild(pictureToLink);
	linkElementWithPicture.href = "#";
	linkElementWithPicture.classList.add(positionInField);
	linkElementWithPicture.classList.add("linkToDetail");
	linkElementWithPicture.setAttribute('onclick','showDetail(this)');
	elementToAppendChild.appendChild(linkElementWithPicture);
}

//FOR EVERY RECORDS REMEMBERS ITS ORDER - ID AND GET TIME FOR SORTING
function prepareDataForSort(parsedXml){
	var recordArray = [];
	//FOR EVERY ITEM - RECORD
	for(i=0; i<items.length; i++){
		var resultDate = parsedXml.evaluate('rss/channel/item['+(i+1)+']/pubDate', parsedXml, null, XPathResult.STRING_TYPE, null);
		//GET DATE FROM STRING IN MILISECOND SINCE 1. 1. 1970 TO COMPARISON	
		newDate = new Date(resultDate.stringValue)
		//CREATE RECORD INSTANCE MAINLY CONTAINING ID TO ABSTRACT INFORMATION FROM XML FILE IN CERTAIN PLACE - ORDER
		recordArray[i]= new Record(i+1,newDate.getTime());		
	}
	return recordArray;
}

//SORTS RECORDS BY ITS TIME IN MILISECONDS
function doSorting(recordArray, decision) {
	//CHOICE OF SORT MODE
	if(decision == "ascending" ) {
		//ASCENDING
		recordArray.sort(function(a,b){ return a.getTimeMs() - b.getTimeMs(); });
	} else {	
		//DESCENDING
		recordArray.sort(function(a,b){ return b.getTimeMs() - a.getTimeMs(); });
	}
}

//REBUILD DETAIL SECTION IF ANOTHER FORMAT USED
function rebuildDetailIfNotAtomUsed() {
	var previousContent = document.getElementsByClassName("descriptionDivWithHtmlContent")[0];
	
	if (previousContent != null) {
		previousContent.innerHTML="";
		var detailPart = document.getElementById("detailPart");
		var linkDetail = document.getElementById("linkDetail");
		var href = linkDetail.getAttribute("href");
		linkDetail.parentElement.removeChild(linkDetail);
		
		if (detailPart != null){
			detailPart.innerHtml="";
		} else {
			detailPart = document.createElement("DIV");
			detailPart.id ="detailPart";
			detailPart.append(document.getElementsByTagName("main")[0]);
		}
	
		var headingDetail = document.createElement("H1");
		headingDetail.id ="headingDetail";
		detailPart.append(headingDetail);
		
		var dateDetail = document.createElement("P");
		dateDetail.id ="dateDetail";
		detailPart.append(dateDetail);
		
		var photoDetail = document.createElement("IMG");
		photoDetail.id ="photoDetail";
		detailPart.append(photoDetail);
		
		var photoDetail = document.createElement("DIV");
		descriptionDetail.id ="descriptionDetail";
		detailPart.append(descriptionDetail);
		
		var descriptionDetailP = document.createElement("P");
		descriptionDetailP.id ="descriptionDetailP";
		descriptionDetail.append(descriptionDetailP);
		
		var linkDetail = document.createElement("A");
		linkDetail.id ="linkDetail";
		linkDetail.href =href;
		linkDetail.alt = "Follow link!";
		detailPart.append(linkDetail);
		
		var followLink = document.createElement("IMG");
		followLink.id ="followLink";
		followLink.src = "rssReaderFiles/pictures/enter.svg";
		linkDetail.append(followLink);
	}
}
//PROCESS XML TEXT - MAIN PART OF ALGORITHM - THEN USES ONLY ITS INSTANCES
function processXMLInTextForm(xmlInTextForm){
	//OBTAINING XML PARSER TO PARSE STRING IN TEXT FORM
	var getXmlParser = new DOMParser();
	//PARSING XML FROM STRING
	parsedXml = getXmlParser.parseFromString(xmlInTextForm, 'text/xml');
	//GET ALL RECORDS - NAMED ITEMS			
	items=parsedXml.getElementsByTagName('item');
	//PREPARES DATA FOR SORT USING PARSED XML
	recordArray = prepareDataForSort(parsedXml);
	//SORTS RECORD BY ITS TIME IN MILISECONDS SINCE 1. 1. 1970
	doSorting(recordArray, document.getElementById("selectOfOrder").value);
	document.getElementById("feeds").innerHTML = "";	
	rebuildDetailIfNotAtomUsed();
	
	var id;
	//FOR EVERY ITEM - RECORD GET ITS DATA USING ID AND WILL BE IN SORTED ORDER
	for(j=0; j<recordArray.length; j++){
		//GETS ID FROM RECORD TO FIND TARGET IN XML
		id = recordArray[j].getId();
		
		//CRETION OF DIV ELEMENT TO ENCAPSULATE RECORD
		var elementDiv = document.createElement("DIV");
		//SET CLASS TO STYLISE DIV
		elementDiv.classList.add("recordOfItem");
		
		//INSERTS FOUND TITLE TO GIVEN RECORD AND INSERTS THIS TO GIVEN DIV
		recordArray[j].setTitle(prepareTitle(id, elementDiv));
		//INSERTS FOUND DATE TO GIVEN RECORD AND INSERTS THIS TO GIVEN DIV	
		recordArray[j].setDate(prepareReadableDate(id, elementDiv));
		//INSERTS FOUND DESCRIPTION TO GIVEN RECORD
		recordArray[j].setDescription(htmlToElementsOrText(prepareDescription(id),recordArray[j], elementDiv));	
		//INSERTS FOUND DATE TO GIVEN RECORD 
		recordArray[j].setImageUrl(prepareImageUrl(id));
		//INSERTS FOUND LINK TO GIVEN RECORD 	
		recordArray[j].setLink(prepareLink(id));
		
		//PREPARE TO DISPLAY DETAILS ON REQUEST AND PREPARES LINK IN ICON		
		prepareLinkToDetailWithPicture(j, elementDiv);
		
		//APPEND THIS ENCAPSULATION OF RECORD TO DIV WITH ID feeds
		document.getElementById("feeds").appendChild(elementDiv);
	}
	
	if( recordArray.length > 0) {
		fillDetail(recordArray[0]);
	}
}

//GET XML USING FETCH API - VIOLATES CORS -> SERVER NEEDED
function getRSSChannelXML(parsedXmlUrl) 
{
	fetch(parsedXmlUrl)
	.then(function(response){
		//get plain text
		return response.text();
	})
		//process this text
	.then(function(data){
		processXMLInTextForm(data);		
	})

}

//USES HTTP REQUEST - OLDER - VIOLATES CORS -> SERVER NEEDED
function getRSSChannelXML1(parsedXmlUrl) 
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', parsedXmlUrl, false);
	xhr.send();
	var getXmlParser = new DOMParser();
		parsedXml = getXmlParser.parseFromString(xhr.responseText, 'text/xml');

		//alert(parsedXml.getElementsByTagName('title').length);
}