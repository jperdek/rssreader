

function getXMLFromServer(urlOfXML) {
	var data = { url: urlOfXML };
	
	if( typeof event != "undefined"){
		event.preventDefault();
	}
	fetch('https://webp.itprof.sk/fetchurl', {
		method: 'POST', // or 'PUT'
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	})
	
	.then(function(response){
			//get plain text
			return response.text();
		})
			//process this text
		.then(function(data){
			processXMLInTextForm(data);		
		})
	.catch((error) => {
		console.log(error);
	});
};

function getXMLFromUrl(urlOfXML) {
	
	if( typeof event != "undefined"){
		event.preventDefault();
	}
	fetch(urlOfXML)
	
	.then(function(response){
			//get plain text
			return response.text();
		})
			//process this text
		.then(function(data){
			processXMLInTextForm(data);		
		})
	.catch((error) => {
		console.log(error);
	});
};

function loadDataOnRequest(){
	var url = document.getElementById('inputPassword2').value;
	var isIE = /*@cc_on!@*/false || !!document.documentMode;
	var isEdge = !isIE && !!window.StyleMedia;
	if( isEdge ){
		document.cookie= "url=" + escape(url);
	} else {
		localStorage.setItem('url', url);
	}
	getXMLFromServer(url);
	//getXMLFromUrl(usedUrl);
}

function initialization() {
	var usedUrl;
	//LAUNCH TEST
	//getXMLFromServer("https://sport.aktuality.sk/rss/hokej/");
	//getXMLFromServer("https://www.tyzden.sk/feed/");
	var isIE = /*@cc_on!@*/false || !!document.documentMode;
	var isEdge = !isIE && !!window.StyleMedia;
	if(! isEdge){
		if( localStorage.getItem('url') == null){
			localStorage.setItem('url', "https://www.sme.sk/rss-title");
			usedUrl = "https://www.sme.sk/rss-title";
		}
		else {
			usedUrl = localStorage.getItem('url');
		}
	}else {
		if( typeof document.cookie == "undefined" || document.cookie == ""){
			document.cookie= "url=https://www.sme.sk/rss-title";
			usedUrl= "https://www.sme.sk/rss-title"
		} else {
			var cookieArray = document.cookie.split("url=");
			usedUrl= unescape(cookieArray[1].split(";")[0]);
		}
	}
	getXMLFromServer(usedUrl);
	//getXMLFromUrl(usedUrl);
}